#!/bin/bash

readonly NAGIOS_OK=0
readonly NAGIOS_WARNING=1
readonly NAGIOS_CRITICAL=2
readonly NAGIOS_UNKNOWN=3

function main {
    local res_name="$1"
    local warn_level="$2"
    local crit_level="$3"

    curlout=$(curl --connect-timeout 5 --max-time 10 -s "http://localhost:2006/$res_name")
    if [ $? -ne 0 ]; then
        echo "UNKNOWN - failed to fetch resource '$res_name' from olsr txtinfo plugin"
        return "$NAGIOS_UNKNOWN"
    fi
    local num_res=$(echo "$curlout" | awk '($0 != "" && NR > 2) { print($1) }' | wc -l)

    local code="$NAGIOS_OK"
    local state="OK"
    local tag=""

    if [ $num_res -le $crit_level ]; then
      code="$NAGIOS_CRITICAL"
      state="CRIT"
      tag="(!!)"
    elif [ $num_res -le $warn_level ]; then
      code="$NAGIOS_WARNING"
      state="WARN"
      tag="(!)"
    fi

    echo "$state - $num_res$tag olsr $res_name found."
    exit "$code"
}

if [ -z "$1" ] || [ -z "$2" ]; then
  echo "UNKNOWN - please specify resource name, warn and critical values"
  exit "$NAGIOS_UNKNOWN"
fi

main "$1" "$2" "$3"
