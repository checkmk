#!/bin/bash

readonly NAGIOS_OK=0
readonly NAGIOS_WARNING=1
readonly NAGIOS_CRITICAL=2
readonly NAGIOS_UNKNOWN=3

function main {
    local file_name="$1"
    local warn_days="$2"
    local crit_days="$3"


    end_date=$(openssl x509 -in "$file_name" -noout -enddate | awk -F '=' '{ print($2) }')
    if [ $? -ne 0 ]; then
        echo "UNKNOWN - failed to read notAfter from certificate $file_name"
        return "$NAGIOS_UNKNOWN"
    fi
    end=$(date -d "$end_date" +%s)
    now=$(date +%s)
    delta_days=$(( (end - now) / (24 * 3600) ))

    local code="$NAGIOS_OK"
    local state="OK"
    local tag=""

    if [ $delta_days -le $crit_days ]; then
      code="$NAGIOS_CRITICAL"
      state="CRIT"
      tag="(!!)"
    elif [ $delta_days -le $warn_days ]; then
      code="$NAGIOS_WARNING"
      state="WARN"
      tag="(!)"
    fi

    echo "$state - certificate will expire in $delta_days$tag days."
    exit "$code"
}

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
  echo "UNKNOWN - please specify certifacte file name, warn and critical days"
  exit "$NAGIOS_UNKNOWN"
fi

main "$1" "$2" "$3"
